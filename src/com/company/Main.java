package com.company;

import com.company.words.Words;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Words words=new Words();
        System.out.println("число повторений: \n" + words.ChisloRepeat());
        System.out.println("уникальные слова в тексте: \n" + words.UnicalWords());
    }
}
