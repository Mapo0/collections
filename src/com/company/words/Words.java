package com.company.words;
import java.io.*;
import java.util.*;

public class Words {
    public String Read() throws IOException {
        File file = new File("text.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String text = "";
        while ((line = br.readLine()) != null) {
            text = text.concat(line);
        }
        br.close();
        fr.close();
        return text;
    }

    public String RefactorString (String s) {
        s = s.toLowerCase()
                .replaceAll("[^A-Za-zА-Яа-я]\\s+", " ");
        return s;
    }

    public Map<String, Long> ChisloRepeat() throws IOException {
        Map<String, Long> map = new HashMap<>();
        for (String s : RefactorString(Read()).split("\\s")) {
            map.put(s, map.containsKey(s) ? map.get(s) + 1 : 1);
        }
        return map;
    }

    public List<String> UnicalWords() throws IOException {
        List<String> entryList = new ArrayList<>();
        for (Map.Entry entry : ChisloRepeat().entrySet()) {
            String key = (String) entry.getKey();
            Long value = (Long) entry.getValue();
            if (value == 1) {
                entryList.add(key);
            }
        }
        return entryList;
    }

}
